import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { QuoteModel } from '../shared/quote.model';
import { QuoteService } from '../shared/quote.service';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent implements OnInit {

  author = '';
  text = '';
  select = '';
  quote: QuoteModel | null = null;
  quoteId!: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private quoteService: QuoteService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.quoteId = params['quote'];
      this.getQuote(this.quoteId);
    });
  }

  getQuote(id: string) {
    this.http.get<QuoteModel>(`https://project-f65ad-default-rtdb.firebaseio.com/quotes/${id}.json`).subscribe(result => {
      this.quote = result;
      if (result) {
        this.author = this.quote.author;
        this.text = this.quote.text;
        this.select = this.quote.category;
      }
    });
  }

  creatQuote() {
    const author = this.author;
    const text = this.text;
    const category = this.select;
    return {author, category, text};
  }

  onClickAddQuote() {
    if (!this.quote) {
      let quote = this.creatQuote();
      this.http.post('https://project-f65ad-default-rtdb.firebaseio.com/quotes.json', quote).subscribe();
    } else {
      let quote = this.creatQuote();
      this.http.put<QuoteModel>(`https://project-f65ad-default-rtdb.firebaseio.com/quotes/${this.quoteId}.json`, quote).subscribe();
    }
    this.quoteService.onChange();
    void this.router.navigate(['/']);
  }
}
