import { EventEmitter } from '@angular/core';

export class QuoteService {
  changeArray = new EventEmitter<void>()
  onChange(){
    this.changeArray.emit();
  }
}
