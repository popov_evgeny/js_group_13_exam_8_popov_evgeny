export class QuoteModel {
  constructor(
    public id: string,
    public author: string,
    public category: string,
    public text: string,
  ) {}
}
