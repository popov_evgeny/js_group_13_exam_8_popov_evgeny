import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { QuoteModel } from '../shared/quote.model';
import { QuoteService } from '../shared/quote.service';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements OnInit {
  arrayQuotes!: QuoteModel[];
  path = '';
  empty = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private quoteService: QuoteService,
  ) { }

  ngOnInit(): void {
    this.quoteService.changeArray.subscribe(() => {
      this.getQuotes(this.path);
    });
    this.route.params.subscribe((params: Params) => {
      this.path = params['quote'];
      this.getQuotes(params['quote'])
      this.empty = '';
    });
  }

  getQuotes(category: string) {
    let path = `?orderBy="category"&equalTo="${category}"`;
    if (category === undefined) {
      path = '';
    }
    this.http.get<{[id: string]: QuoteModel}>(`https://project-f65ad-default-rtdb.firebaseio.com/quotes.json${path}`)
      .pipe(map(result => {
        if(result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const quote = result[id];
          return new QuoteModel(id, quote.author, quote.category, quote.text);
        });
      }))
      .subscribe(quotes =>{
        this.arrayQuotes = [];
        this.arrayQuotes = quotes;
        if (this.arrayQuotes.length === 0) {
          this.empty = 'This server haven`t quotes!';
        }
      });
  }
  onClickEdit(id: string) {
    if (this.path === undefined){
      void this.router.navigate([ 'quotes', id, 'edit']);
    } else {
      void this.router.navigate([ 'quotes', this.path, id, 'edit']);
    }

  }

  onClickDeleteQuote(id: string) {
    this.http.delete(`https://project-f65ad-default-rtdb.firebaseio.com/quotes/${id}.json`).subscribe();
    this.getQuotes(this.path);
    this.quoteService.onChange();
    void this.router.navigate(['/']);
  }
}
