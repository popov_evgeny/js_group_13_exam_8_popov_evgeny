import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { QuoteComponent } from './quote/quote.component';
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
  { path: '', component: HomeComponent, children: [
      { path: '', component: QuoteComponent},
      { path: ':quotes/:quote', component: QuoteComponent},
    ]},
  { path: 'quotes/:quotes/:quote/:edit', component: NewQuoteComponent},
  { path: 'quotes/:quote/:edit', component: NewQuoteComponent},
  { path: 'add', component: NewQuoteComponent},
  { path: '**', component: NotFoundComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
